import java.io.*;
import java.util.*;

public class BlueDevil extends Person{
	
	private String studentID;
	

	//A default constructor that takes no argument. 
	public BlueDevil() {
		this.name = "";
		this.studentID = "";
		init();
	}
	
	//Takes 5 string arguments to specify name, gender, national origin, and degree_department, and DukeID.
	//Gender must be "M", "m", "F", or "f", otherwise throw exception.	
	public BlueDevil(String name, String gender, String origin, String degree_department, String DukeID) {
		//Check whether gender is in valid format. 
		if (gender.equals("M") ||gender.equals("m")) {
			this.gender = "M";
		} else if (gender.equals("F") ||gender.equals("f")) {
			this.gender = "F";
		} else {
			throw new IllegalArgumentException("Gender is in invalid format.");
		}
		
		this.name = name;
		this.origin = origin;
		this.degree_department = degree_department;
		this.studentID = DukeID;
		//Initialize the rest
		init();
	}
	
	public void addDukeID(String id) {
		this.studentID = id;
	}
	
	public String getDukeID() {
		return this.studentID;
	}
}
