import java.io.*;
import java.util.*;

public class ECE_651 {
	
	//Each instance of ECE_651 class has only one professor. 
	//But can have multiple TAs and students. 
	private BlueDevil professor;
	private List<BlueDevil> TAs;
	private List<BlueDevil> students;

	
	//A Default constructor that takes no arguments.  
	public ECE_651() {
		this.professor = new BlueDevil();
		this.TAs = new ArrayList<BlueDevil>();
		this.students = new ArrayList<BlueDevil>();
	}
	
	//A constructor that takes 
	public ECE_651(BlueDevil professor) {
		this.professor = professor;
		this.TAs = new ArrayList<BlueDevil>();
		this.students = new ArrayList<BlueDevil>();
	}
	
	//To add an arbitrary number of TAs to the class. 
	public void addTAs(BlueDevil... theTAs) {
		for (BlueDevil s : theTAs) this.TAs.add(s);
	}
	
	//To add an arbitrary number of students to the class. 
	public void addStudents(BlueDevil... theStudents) {
		for (BlueDevil s : theStudents) this.students.add(s);
	}
	
	public String whoIs(String name) {
		String info = "Here is the information about " + name + ": ";
		//Search through all people in the class. 
		BlueDevil curr = null; 
		String gen1 = "";
		String gen2 = "";
		String position = "";
		if (name.equals(professor.getName())) {
			curr = professor;
			position = "professor";
		} else {
			//Basic Assumption: TAs and students have distinct names. 
			for (int i = 0; i < TAs.size(); i++) {
				if (name.equals(TAs.get(i).getName())) {
					curr = TAs.get(i);
					position = "TA";
				}
			}
			for (int i = 0; i < students.size(); i++) {
				if (name.equals(students.get(i).getName())) {
					curr = students.get(i);
					position = "student";
				}
			}			
		}
		
		//If the person does not exist in this class:
		if (curr == null) {
			info = info + "This person is not in this class.";
			return info;
		}
		
		//This person is in this class. Get the information of this person. 
		if (curr.getGender().equals("M")) {
			gen1 = "He"; gen2 = "His";
		} else if (curr.getGender().equals("F")) {
			gen1 = "She"; gen2 = "Her";
		} else {
			gen1 = "He/She"; gen2 = "His/Her";
		}
		
		info = info + gen1 + " is a " + position + " of this ECE 651 class. ";
		if (position.equals("TA") || position.equals("student")) {
			String temp1 = curr.getDegreeDepartment().equals("") ? gen1 + " did not specify " + gen2 + " degree program." : gen1 + " is a " + curr.getDegreeDepartment() + ". ";
			info = info + temp1;
		}
		
		//Duke ID
		if (curr.getDukeID().equals("")) {
			info = info + gen1 + " did not tells us his DukeID. ";
		} else {
			info = info + gen2 + " DukeID is " + curr.getDukeID() + ". ";
		}
		
		//National origin. 
		String temp = curr.getOrigin().equals("") ? gen1 + " did not specify where " + gen1 + " is from. " : gen1 + " is from " + curr.getOrigin() + ". ";
		info = info + temp;
		
		//Work experience. 
		temp = gen1 + " has " + curr.getWorkExperience() + " year(s) of work experience. ";
		info = info + temp;		
		
		//College. 
		temp = curr.getCollege().equals("") ? gen1 + " did not specify " + gen2 + " college. " : gen1 + " graduated from " + curr.getCollege() + ". ";
		info = info + temp;
		
		//Hobbies
		if (curr.getHobbies().size() == 0) {
			info = info + gen1 + " did not tell us " +  gen2 + " hobbies. ";
			return info;
		} else {
			String h = gen2 + " hobbies include: ";
			for (int i = 0; i < curr.getHobbies().size() - 1; i++) {
				h = h + curr.getHobbies().get(i) + ", ";
			}
			h = h + curr.getHobbies().get(curr.getHobbies().size() - 1) + ". ";
			return info + h;
		}
	}
}
