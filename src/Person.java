import java.io.*;
import java.util.*;


public class Person {
	protected String name;
	protected String gender;
	protected String origin;
	protected String degree_department;
	protected int work_experience;
	protected String college;
	protected List<String> hobbies;
	
	
	//Default constructor that takes no arguments. 
	public Person() {
		name = "";
		gender = "";
		origin = "";
		degree_department = "";
		init();
	}
	
	//Takes 4 string arguments to specify name, gender, national origin, and degree_department.
	//Gender must be "M", "m", "F", or "f", otherwise throw exception.
	public Person(String name, String gender, String origin, String degree_department) {
		//Check whether gender is in valid format. 
		if (gender.equals("M") ||gender.equals("m")) {
			this.gender = "M";
		} else if (gender.equals("F") ||gender.equals("f")) {
			this.gender = "F";
		} else {
			throw new IllegalArgumentException("Gender is in invalid format.");
		}
		
		this.name = name;
		this.origin = origin;
		this.degree_department = degree_department;
		//Initialize the rest
		init();
	}
	
	protected void init() {
		work_experience = 0;
		college = "";
		hobbies = new ArrayList<String>();
	}
	
	//A series of adding function to add information about the person. 
	public void addName(String name) {this.name = name;}
	public void addOrigin(String origin) {this.origin = origin;}
	public void addDegree_Department(String dd) {this.degree_department = dd;}
	public void addCollege(String coll) {this.college = coll;}
	public void addWorkExperience(int year) {
		if (year < 0) throw new IllegalArgumentException("Work experience must be >= 0!");
		this.work_experience = year;
	};
	public void addGender(String gender) {
		if (gender.equals("M") ||gender.equals("m")) {
			this.gender = "M";
		} else if (gender.equals("F") ||gender.equals("f")) {
			this.gender = "F";
		} else {
			throw new IllegalArgumentException("Gender is in invalid format.");
		}		
	}
	
	//Add an arbitrary number of hobbies. 
	public void addHobbit(String... hobbyList) {
		for (String s : hobbyList) this.hobbies.add(s);
	}
	
	//A series of functions that return specified information about the person.
	public String getName() {return this.name;}
	public String getGender() {return this.gender;}	
	public String getOrigin() {return this.origin;}	
	public String getDegreeDepartment() {return this.degree_department;}	
	public String getCollege() {return this.college;}	
	public int getWorkExperience() {return this.work_experience;};
	public List<String> getHobbies() {return this.hobbies;}
}
