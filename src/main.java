import java.io.*;
import java.util.*;

public class main {

	public static void main(String[] args) {
		
		//Create information about Professor Telford and create an new instance of ECE_651.
		//Add the information of the professor into the class.
		BlueDevil ric = new BlueDevil("Richard Telford", "M", "America", "", "rt113");
		ric.addCollege("Trinity University");
		ric.addWorkExperience(30);
		ECE_651 sectionOne = new ECE_651(ric);
		System.out.println(sectionOne.whoIs("Richard Telford"));
		
		
		
		//Add information about myself to the class.
		BlueDevil cheng = new BlueDevil("Cheng Xing", "M", "China", "ECE MS Candidate", "cx30");
		cheng.addHobbit("reading", "cooking");
		cheng.addCollege("University of Washington");
		sectionOne.addStudents(cheng);
		System.out.println(sectionOne.whoIs("Cheng Xing"));
		
		//Add information about the three TAs to the class. 
		BlueDevil shalin = new BlueDevil("Shalin Shah", "M", "India", "ECE PhD candidate", "sns37");
		shalin.addWorkExperience(0);
		shalin.addCollege("DA-IICT"); shalin.addHobbit("bodybuilding", "dancing");
		BlueDevil anli = new BlueDevil("Anli Ganti", "M", "New Jersey", "ECE PhD candidate", "ag403");
		BlueDevil niral = new BlueDevil("Niral Shal", "M", "New Jersey", "ECE MENG student", "ns247");
		niral.addCollege("Rutgers University");
		
		sectionOne.addTAs(shalin); sectionOne.addTAs(anli); sectionOne.addTAs(niral); 
		System.out.println(sectionOne.whoIs("Shalin Shah"));
		System.out.println(sectionOne.whoIs("Anli Ganti"));
		System.out.println(sectionOne.whoIs("Niral Shal"));
		
		
	}
}
